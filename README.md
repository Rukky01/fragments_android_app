# Fragments_Android_App
#### This android app is created as a faculty project and as such has no real use-case just to show learned skill.
It features four different fragments inside ViewPager.

### Developed
This project is developed by [Luka Ruskan.](https://lukaruskan.com/)
### Preview
![Preview](preview.png)


