package com.demo.fragments;

public interface ChangePageListener {
    void changePage(int pageId);
}
