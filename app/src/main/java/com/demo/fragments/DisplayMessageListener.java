package com.demo.fragments;

public interface DisplayMessageListener {
    void setMessage(String message);
}
