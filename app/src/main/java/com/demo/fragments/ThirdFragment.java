package com.demo.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ThirdFragment extends Fragment {

    private String type;
    private FrameLayout frameLayout;

    public ThirdFragment(String type) {
        this.type = type;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_third, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        frameLayout=view.findViewById(R.id.thirdFragment);
        if(type.equals("image")) createImageView();
        else if(type.equals("text")) createTextView();
    }

    @SuppressLint("SetTextI18n")
    private void createTextView(){
        TextView textView=new TextView(getContext());
        textView.setText("Hello World!");
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(layoutParams);
        layoutParams.gravity= Gravity.CENTER;
        textView.setLayoutParams(layoutParams);
        frameLayout.addView(textView);
        textView.setTextSize(24);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void createImageView(){
        ImageView imageView= new ImageView(getContext());
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.android));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity=Gravity.CENTER;
        imageView.setLayoutParams(layoutParams);
        frameLayout.addView(imageView);
    }

}