package com.demo.fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.Locale;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter implements DisplayMessageListener{

    private static final int NUM_PAGES = 4;
    private static final String BASE_NAME = "Tab #%d";
    private final FirstFragment firstFragment;
    private final SecondFragment secondFragment;
    private final ChangePageListener changePageListener;

    public ScreenSlidePagerAdapter(@NonNull FragmentManager fm, ChangePageListener changePageListener) {
        super(fm);
        firstFragment = new FirstFragment(this);
        secondFragment = new SecondFragment();
        this.changePageListener = changePageListener;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return firstFragment;
            case 1:
                return secondFragment;
            case 2:
                return new ThirdFragment("text");
            default:
                return new ThirdFragment("image");
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return String.format(Locale.getDefault(), BASE_NAME, position + 1);
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }


    @Override
    public void setMessage(String message) {
        secondFragment.displayMessage(message);
        changePageListener.changePage(1);
    }
}
