package com.demo.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Objects;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class FirstFragment extends Fragment {

    private EditText etInput;
    private final DisplayMessageListener displayMessageListener;

    public FirstFragment(DisplayMessageListener displayMessageListener) {
        this.displayMessageListener = displayMessageListener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etInput = view.findViewById(R.id.etInput);
        Button bSubmit = view.findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
                hideKeyboard();
                etInput.setText("");
            }
        });
    }

    private void buttonClick(){
        displayMessageListener.setMessage(etInput.getText().toString());
    }

    private void hideKeyboard (){
        try {
            InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity()).getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(Objects.requireNonNull(getView()).getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}